lines = open("input.txt") do io
    readlines(io)
end

# time to start using functions!

# PART 1
function is_one_the_most_common_bit(lines, i)
    # find whether 1 is the most common bit in `lines` at position `i`
    # returns `true` if 1 is either the most common bit or equally common as 0
    ones = mapreduce(x -> parse(Bool, x[i]), +, lines)
    return ones >= length(lines) / 2
end

function compute_gamma_rate(lines, num_bits)
    gamma_rate = 0

    for i in 1:num_bits
        one_most_common = is_one_the_most_common_bit(lines, i)

        # if 1 is the most common bit, place a binary 1 at the correct position
        gamma_rate += one_most_common * 2 ^ (num_bits - i)
    end

    return gamma_rate
end

function compute_epsilon_rate(gamma_rate, num_bits)
    # we can compute the epsilon rate quickly as it's a bitwise NOT(gamma_rate)
    2 ^ (num_bits) - 1 - gamma_rate
end

function first_challenge()
    num_bits = length(lines[1])
    gamma_rate = compute_gamma_rate(lines, num_bits)
    epsilon_rate = compute_epsilon_rate(gamma_rate, num_bits)

    println("What is the power consumption of the submarine?")
    println(gamma_rate * epsilon_rate)
end

# PART 2
function compute_oxygen_generator_rating(lines, num_bits)
    for i in 1:num_bits
        one_most_common = is_one_the_most_common_bit(lines, i)

        # only keep values with the most common bit in position i
        # equally common -> keep 1
        lines = filter(x -> parse(Bool, x[i]) == one_most_common, lines)

        if length(lines) == 1
            return parse(Int, lines[1], base=2)
        end
    end
end

function compute_co2_scrubber_rating(lines, num_bits)
    for i in 1:num_bits
        one_most_common = is_one_the_most_common_bit(lines, i)

        # only keep values with the least common bit in position i
        # equally common -> keep 0
        lines = filter(x -> parse(Bool, x[i]) != one_most_common, lines)

        if length(lines) == 1
            return parse(Int, lines[1], base=2)
        end
    end
end

function second_challenge()
    num_bits = length(lines[1])
    oxygen_generator_rating = compute_oxygen_generator_rating(lines, num_bits)
    co2_scrubber_rating = compute_co2_scrubber_rating(lines, num_bits)

    println("What is the life support rating of the submarine?")
    println(oxygen_generator_rating * co2_scrubber_rating)
end

# run it all!
first_challenge()
second_challenge()
