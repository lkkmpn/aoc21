@enum AmphipodType A B C D
@enum State in_incorrect_room in_hallway in_correct_room

struct Amphipod
    type::AmphipodType
    state::State
    coordinates::NTuple{2, Int8}
end

const TYPES = Dict('A' => A, 'B' => B, 'C' => C, 'D' => D)
const COST = Dict(A => 1, B => 10, C => 100, D => 1000)
const TARGET_X = Dict(A => 4, B => 6, C => 8, D => 10)
const HALLWAY_X = [2, 3, 5, 7, 9, 11, 12]
const HALLWAY_Y = 2

function main()
    organise(parse_initial_config("input1.txt"))
    organise(parse_initial_config("input2.txt"))
end

function parse_initial_config(filename)
    lines = open(filename) do io
        readlines(io)
    end

    config = Set{Amphipod}()

    for line in [3, 4, 5, 6]
        if length(lines) < line
            continue
        end
        for char in [4, 6, 8, 10]
            if !(lines[line][char] in ['A', 'B', 'C', 'D'])
                continue
            end
            type = TYPES[lines[line][char]]
            state = TARGET_X[type] == char && line == 4 ? in_correct_room : in_incorrect_room
            coordinates = (char, line)
            amphipod = Amphipod(type, state, coordinates)
            push!(config, amphipod)
        end
    end

    return config
end

function organise(initial_config)
    min_costs = Dict(initial_config => 0)
    queue = [initial_config]

    while length(queue) > 0
        config = popfirst!(queue)
        config_cost = min_costs[config]
        next_configs = generate_next_steps(config)

        for next_config in keys(next_configs)
            step_cost = next_configs[next_config]

            if next_config in keys(min_costs)
                if min_costs[next_config] > config_cost + step_cost
                    min_costs[next_config] = config_cost + step_cost
                end
            else
                min_costs[next_config] = config_cost + step_cost
                push!(queue, next_config)
            end
        end
    end

    final_keys = filter(c -> length(filter(a -> a.state == in_correct_room, c)) == length(initial_config), keys(min_costs))

    println("What is the least energy required to organize the amphipods?")
    println(min_costs[final_keys...])
end

function generate_next_steps(config)
    next_configs = Dict{Set, Int}()
    occupied = Dict(map(a -> a.coordinates => a.type, [config...]))

    for amphipod in config
        destinations = get_possible_destinations(amphipod, occupied)

        for dest in destinations
            next_config = copy(config)
            delete!(next_config, amphipod)
            state = dest[2] == HALLWAY_Y ? in_hallway : in_correct_room
            push!(next_config, Amphipod(amphipod.type, state, dest))
            step_cost = distance(amphipod.coordinates, dest) * COST[amphipod.type]
            next_configs[next_config] = step_cost
        end
    end

    return next_configs
end

function get_possible_destinations(amphipod, occupied)
    occupied_coordinates = keys(occupied)
    destinations = NTuple{2, Int8}[]

    if amphipod.state == in_correct_room
        return destinations
    end

    if amphipod.state == in_incorrect_room  # to hallway
        for x in HALLWAY_X
            coordinates = (x, HALLWAY_Y)
            if path_is_clear(amphipod.coordinates, coordinates, occupied_coordinates)
                push!(destinations, coordinates)
            end
        end
    end
    if amphipod.state == in_incorrect_room || amphipod.state == in_hallway  # to correct room
        ys = (length(occupied) == 16) ? [6, 5, 4, 3] : [4, 3]  # bodged way to check part

        # check in reverse
        for y in ys
            dest = (TARGET_X[amphipod.type], y)
            below_dest = (TARGET_X[amphipod.type], y + 1)
            if y < ys[1] &&  # check type below, must be occupied based on the if-statement below
                occupied[below_dest] != amphipod.type
                break
            end
            if !(dest in occupied_coordinates)
                if path_is_clear(amphipod.coordinates, dest, occupied_coordinates)
                    push!(destinations, dest)
                end
                break
            end
        end
    end

    return destinations
end

function path_is_clear(from, to, occupied)
    path = NTuple{2, Int8}[]
    if from[2] > HALLWAY_Y  # first move to hallway
        for y in from[2]:-1:HALLWAY_Y
            push!(path, (from[1], y))
        end
    end
    # move horizontally
    for x in min(from[1], to[1]):max(from[1], to[1])
        push!(path, (x, HALLWAY_Y))
    end
    if to[2] > HALLWAY_Y  # move to room
        for y in HALLWAY_Y:to[2]
            push!(path, (to[1], y))
        end
    end
    for coord in path
        if coord in occupied && coord != from
            return false
        end
    end
    return true
end

function distance(from, to)
    # first to hallway, then through hallway, then to room
    abs(2 - from[2]) + abs(to[1] - from[1]) + abs(to[2] - 2)
end

main()
