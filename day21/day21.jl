mutable struct Player
    position::Int
    score::Int
end

mutable struct Die
    last_roll::Int
    num_rolls::Int
end

const TRACK_LENGTH = 10
const WIN_SCORE = 1000
const DIE_SIDES = 100
const DIE_ROLLED = 3

const PAR_WIN_SCORE = 21

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    initial_positions = parse_positions(lines)

    # PART 1
    practice_game(initial_positions)

    # PART 2
    parallel_universes(initial_positions)
end

function parse_positions(lines)
    map(l -> parse(Int, split(l, " ")[end]), lines)
end

function practice_game(initial_positions)
    players = map(pos -> Player(pos, 0), initial_positions)

    players, die = play!(players)

    losing_player_score = minimum(map(p -> p.score, players))
    println("The moment either player wins, what do you get if you multiply the score of the losing player by the number of times the die was rolled during the game?")
    println(die.num_rolls * losing_player_score)
end

function play!(players)
    die = Die(0, 0)

    while true
        for player in players
            roll!(player, die)
            if player.score >= WIN_SCORE
                return players, die
            end
        end
    end
end

function roll!(player, die)
    if die.last_roll > DIE_SIDES - DIE_ROLLED
        num_over = die.last_roll - (DIE_SIDES - DIE_ROLLED)
        moves = (die.last_roll + 2) * 3 - DIE_SIDES * num_over
    else
        moves = (die.last_roll + 2) * 3
    end

    player.position = (player.position + moves - 1) % TRACK_LENGTH + 1
    player.score += player.position

    die.last_roll = (die.last_roll + DIE_ROLLED - 1) % DIE_SIDES + 1
    die.num_rolls += DIE_ROLLED
end

function parallel_universes(initial_positions)
    # this needs a smarter approach
    # a game is uniquely identified by the tuple (P1 position, P1 score, P2 position, P2 score)
    # we can keep track of the number of games in each unique configuration
    # note that the player playing in each universe is the same, so we can globally keep track of that
    num_games = Dict{NTuple{4, Int8}, Int}()

    # initialise one game
    initial_state = (initial_positions[1], 0, initial_positions[2], 0)
    num_games[initial_state] = 1

    done = false
    player = 1
    while !done
        new_num_games = Dict{NTuple{4, Int8}, Int}()
        won_states = 0
        for state in keys(num_games)
            if state[2] >= PAR_WIN_SCORE || state[4] >= PAR_WIN_SCORE
                # already won game
                won_states += 1
                add_to_dict_key!(new_num_games, state, num_games[state])  # don't change
                continue
            end

            # advance this state into 27 new states
            new_states = generate_states(state, player)
            for new_state in new_states
                add_to_dict_key!(new_num_games, new_state, num_games[state])
            end
        end
        num_games = new_num_games
        player = (player == 1) ? 2 : 1  # switch player
        if won_states == length(num_games)
            break
        end
    end

    player_wins = [0, 0]
    for state in keys(num_games)
        if state[2] >= PAR_WIN_SCORE
            player_wins[1] += num_games[state]
        elseif state[4] >= PAR_WIN_SCORE
            player_wins[2] += num_games[state]
        end
    end

    println("Find the player that wins in more universes; in how many universes does that player win?")
    println(maximum(player_wins))
end

function generate_states(state, player)
    player_indices = (player == 1) ? [1, 2] : [3, 4]
    other_indices = (player == 1) ? [3, 4] : [1, 2]
    position, score = state[player_indices]

    new_states = []
    for a in 1:3, b in 1:3, c in 1:3
        total_roll = a + b + c
        new_position = (position + total_roll - 1) % TRACK_LENGTH + 1
        new_score = score + new_position

        if player == 1
            new_state = (new_position, new_score, state[other_indices]...)
        elseif player == 2
            new_state = (state[other_indices]..., new_position, new_score)
        end
        push!(new_states, new_state)
    end
    return new_states
end

function add_to_dict_key!(d, k, v)
    if k in keys(d)
        d[k] += v
    else
        d[k] = v
    end
end

main()
