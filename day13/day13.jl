const NEWLINE = "\r\n"  # note: input txt has CRLF

struct Point
    x::Int
    y::Int
end

struct Fold
    direction::Char
    position::Int
end

function main()
    input = open("input.txt") do io
        read(io, String)
    end

    dots, folds = get_instructions(input)

    # PART 1
    first_fold(dots, folds)

    # PART 2
    code(dots, folds)
end

function get_instructions(input)
    dots_input, folds_input = split(input, NEWLINE * NEWLINE)

    dots = Set{Point}()
    for line in split(dots_input, NEWLINE)
        if isempty(line)
            continue
        end
        push!(dots, parse_dot_line(line))
    end

    folds = Fold[]
    for line in split(folds_input, NEWLINE)
        if isempty(line)
            continue
        end
        push!(folds, parse_fold_line(line))
    end

    return dots, folds
end

function parse_dot_line(line)
    Point(parse.(Int, split(line, ','))...)
end

function parse_fold_line(line)
    text, position = split(line, '=')
    if 'x' in text
        direction = 'x'
    elseif 'y' in text
        direction = 'y'
    end
    return Fold(direction, parse(Int, position))
end

function first_fold(initial_dots, folds)
    dots = copy(initial_dots)
    perform_folds!(dots, folds, 1)

    println("How many dots are visible after completing just the first fold instruction on your transparent paper?")
    println(length(dots))
end

function perform_folds!(dots, folds, num=0)
    for (i, fold) in enumerate(folds)
        for dot in dots
            coordinate = getfield(dot, Symbol(fold.direction))
            if coordinate > fold.position
                new_coordinate = fold.position - (coordinate - fold.position)
                if fold.direction == 'x'
                    new_dot = Point(new_coordinate, dot.y)
                elseif fold.direction == 'y'
                    new_dot = Point(dot.x, new_coordinate)
                end
                delete!(dots, dot)
                push!(dots, new_dot)
            end
        end

        if i == num
            break
        end
    end
end

function code(initial_dots, folds)
    dots = copy(initial_dots)
    perform_folds!(dots, folds)

    println("What code do you use to activate the infrared thermal imaging camera system?")
    print_code(dots)
end

function print_code(dots)
    # convert dots into a string and print it
    dots = [dots...]
    max_x = maximum(map(p -> p.x, dots))
    max_y = maximum(map(p -> p.y, dots))

    matrix = falses(max_y + 1, max_x + 1)  # 0-indexed -> 1-indexed
    for dot in dots
        matrix[dot.y + 1, dot.x + 1] = true
    end

    # build string
    code = ""
    for y in 1:max_y + 1
        for x in 1:max_x + 1
            code *= matrix[y, x] ? '█' : ' '
        end
        code *= '\n'
    end

    println(code)
end

main()
