function main()
    line = open("input.txt") do io
        read(io, String)
    end

    input = parse.(Int, split(line, ","))

    # PART 1
    get_minimum_fuel(input, get_total_fuel_linear)
    # PART 2
    get_minimum_fuel(input, get_total_fuel_triangular)
end

function get_minimum_fuel(positions, cost_function)
    # while you could do this all fancy with math,
    # we're just going to brute force it.
    # the optimal horizontal position is between the
    # minimum and maximum horizontal positions in the input
    min_pos, max_pos = extrema(positions)
    min_fuel = typemax(Int)

    for final_pos in min_pos:max_pos
        fuel = cost_function(positions, final_pos)
        if fuel < min_fuel
            min_fuel = fuel
        end
    end

    println("How much fuel must they spend to align to that position?")
    println(min_fuel)
end

function get_total_fuel_linear(initial_positions, final_pos)
    sum(abs.(initial_positions .- final_pos))
end

function get_total_fuel_triangular(initial_positions, final_pos)
    dist = abs.(initial_positions .- final_pos)
    sum(Int.(dist .* (dist .+ 1) ./ 2))
end

main()
