lines = open("input.txt") do io
    readlines(io)
end

# PART 1
horizontal = 0
depth = 0
for line in lines
    dir, dist = split(line)
    dist = parse(Int, dist)
    if dir == "forward"
        global horizontal += dist
    elseif dir == "up"
        global depth -= dist
    elseif dir == "down"
        global depth += dist
    else
        println("something wrong :(")
    end
end

println("What do you get if you multiply your final horizontal position by your final depth?")
println(horizontal * depth)

# PART 2
horizontal = 0
depth = 0
aim = 0
for line in lines
    dir, dist = split(line)
    dist = parse(Int, dist)
    if dir == "forward"
        global horizontal += dist
        global depth += aim * dist
    elseif dir == "up"
        global aim -= dist
    elseif dir == "down"
        global aim += dist
    else
        println("something wrong :(")
    end
end

println("What do you get if you multiply your final horizontal position by your final depth?")
println(horizontal * depth)
