const OPEN_CHARS = ['(', '[', '{', '<']
const CLOSE_CHARS = [')', ']', '}', '>']
const FORWARD_CHAR_DICT = Dict(OPEN_CHARS .=> CLOSE_CHARS)
const REVERSE_CHAR_DICT = Dict(CLOSE_CHARS .=> OPEN_CHARS)
const SYNTAX_ERROR_SCORE = Dict(
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25137
)
const AUTOCOMPLETE_SCORE = Dict(
    ')' => 1,
    ']' => 2,
    '}' => 3,
    '>' => 4
)

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    # PART 1
    score_corrupted_lines(lines)

    # PART 2
    middle_score_incomplete_lines(lines)
end

function score_corrupted_lines(lines)
    total_score = sum(map(get_incorrect_character_score,
                          filter(is_corrupted, lines)))

    println("What is the total syntax error score for those errors?")
    println(total_score)
end

function is_corrupted(line)
    stack = Char[]
    for char in line
        if char in OPEN_CHARS
            push!(stack, char)
        elseif char in CLOSE_CHARS
            if stack[end] == REVERSE_CHAR_DICT[char]  # pair matches
                pop!(stack)
            else  # pair doesn't match
                return true
            end
        end
    end
    return false
end

function get_incorrect_character_score(line)
    # return the score of the first incorrect character in a corrupted line
    stack = Char[]
    for char in line
        if char in OPEN_CHARS
            push!(stack, char)
        elseif char in CLOSE_CHARS
            if stack[end] == REVERSE_CHAR_DICT[char]  # pair matches
                pop!(stack)
            else  # pair doesn't match, syntax error
                return SYNTAX_ERROR_SCORE[char]
            end
        end
    end
    return 0  # if no syntax errors
end

function middle_score_incomplete_lines(lines)
    scores = map(get_incomplete_line_score,
                 filter(!is_corrupted, lines))

    println("What is the middle score?")
    println(sort(scores)[Int(ceil(length(scores) / 2))])
end

function get_incomplete_line_score(line)
    # return the score of the characters added to an incomplete line
    stack = Char[]
    for char in line
        if char in OPEN_CHARS
            push!(stack, char)
        elseif char in CLOSE_CHARS
            if stack[end] == REVERSE_CHAR_DICT[char]  # pair matches
                pop!(stack)
            end  # all pairs match in incomplete lines
        end
    end
    # the stack now contains the characters that should be closed (in reverse)
    score = 0
    for char in reverse(stack)
        score *= 5
        score += AUTOCOMPLETE_SCORE[FORWARD_CHAR_DICT[char]]
    end
    return score
end

main()
