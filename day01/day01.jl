# run code within block and return to lines variable
lines = open("input.txt") do io
    readlines(io)
end

# convert all values to ints
# the dot after the function name vectorises the function
# https://docs.julialang.org/en/v1/manual/functions/#man-vectorized
lines = parse.(Int, lines)

# PART 1
# count every time the next value is higher (depth measurement increases)
# note that Julia is 1-indexed and the range notation includes the final value
counter = 0
for i = 1:length(lines)-1
    diff = lines[i + 1] - lines[i]
    if diff > 0
        global counter += 1
    end
end

println("How many measurements are larger than the previous measurement?")
println(counter)

# PART 2
# now count based on three measurements
counter = 0
for i = 1:length(lines)-3
    measurement1 = lines[i] + lines[i + 1] + lines[i + 2]
    measurement2 = lines[i + 1] + lines[i + 2] + lines[i + 3]
    diff = measurement2 - measurement1
    if diff > 0
        global counter += 1
    end
end

println("How many sums are larger than the previous sum?")
println(counter)
