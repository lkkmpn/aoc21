function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    risk_levels = generate_risk_level_map(lines)

    part_one(risk_levels)
    part_two(risk_levels)
end

function generate_risk_level_map(lines)
    cols = length(lines[1])
    rows = length(lines)

    risk_levels = Matrix{Int}(undef, rows, cols)
    for i = 1:rows
        risk_levels[i, :] = parse.(Int, collect(lines[i]))
    end

    return risk_levels
end

function part_one(risk_levels)
    lowest_risk = dijkstra(risk_levels)

    println("What is the lowest total risk of any path from the top left to the bottom right?")
    println(lowest_risk)
end

function part_two(risk_levels)
    risk_levels = generate_larger_map(risk_levels)
    lowest_risk = dijkstra(risk_levels)

    println("Using the full map, what is the lowest total risk of any path from the top left to the bottom right?")
    println(lowest_risk)
end

function dijkstra(risk_levels)
    indices = CartesianIndices(risk_levels)
    source, target = first(indices), last(indices)

    # Dijkstra
    unvisited = Set(CartesianIndices(risk_levels))
    delete!(unvisited, source)  # source

    # minimum total risk between source and point
    total_risks = fill(typemax(Int), size(risk_levels))
    total_risks[source] = 0

    current = source

    while length(unvisited) > 1
        # calculate total risk levels of neighbours
        neighbours = get_neighbours(current, size(risk_levels))
        for neighbour in filter(n -> n in unvisited, neighbours)
            risk_through_current = total_risks[current] + risk_levels[neighbour]
            if risk_through_current < total_risks[neighbour]
                total_risks[neighbour] = risk_through_current
            end
        end

        # mark current node as visited
        delete!(unvisited, current)

        if current == target
            break
        end

        # find next node: minimum total risk of unvisited nodes
        unvisited_risks = Dict(map(i -> (i, total_risks[i]), [unvisited...]))
        current = findmin(unvisited_risks)[2]
    end

    return total_risks[target]
end

function generate_larger_map(risk_levels)
    larger_map = Matrix{Int}(undef, (size(risk_levels) .* 5)...)
    square_size = size(risk_levels)[1]

    for offset in CartesianIndex(1, 1):CartesianIndex(5, 5)
        top_left = CartesianIndex((Tuple(offset) .- 1) .* square_size .+ 1)
        bottom_right = CartesianIndex(Tuple(offset) .* square_size)
        risk_delta = offset[1] + offset[2] - 2
        this_risk_levels = risk_levels .+ risk_delta
        this_risk_levels = map(r -> (r > 9) ? r - 9 : r, this_risk_levels)
        larger_map[top_left:bottom_right] = this_risk_levels
    end

    return larger_map
end

function get_neighbours(index, size)
    ndims = length(size)
    neighbours = CartesianIndex{ndims}[]
    for i = 1:ndims
        unit = zeros(Int, ndims)
        unit[i] = 1
        unit = CartesianIndex(Tuple(unit))
        if index[i] > 1
            push!(neighbours, index - unit)
        end
        if index[i] < size[i]
            push!(neighbours, index + unit)
        end
    end
    return neighbours
end

main()
