mutable struct Probe
    x::Int
    y::Int
    vx::Int
    vy::Int
end

struct Box
    xmin::Int
    xmax::Int
    ymin::Int
    ymax::Int
end

function main()
    line = open("input.txt") do io
        read(io, String)
    end

    target = parse_target_area(line)

    vxmin = 0
    vxmax = target.xmax
    vymin = target.ymin
    vymax = 200

    maxy = 0
    hits_count = 0

    for vx0 in vxmin:vxmax, vy0 in vymin:vymax
        target_hit, this_maxy = simulate_trajectory(vx0, vy0, target)
        if target_hit
            hits_count += 1
            if this_maxy > maxy
                maxy = this_maxy
            end
        end
    end

    println("What is the highest y position it reaches on this trajectory?")
    println(maxy)

    println("How many distinct initial velocity values cause the probe to be within the target area after any step?")
    println(hits_count)
end

function parse_target_area(line)
    x, y = split(split(line, ": ")[2], ", ")
    xmin, xmax = parse.(Int, split(x[3:end], ".."))
    ymin, ymax = parse.(Int, split(y[3:end], ".."))
    return Box(xmin, xmax, ymin, ymax)
end

function simulate_trajectory(vx0, vy0, target)
    maxy = 0
    probe = Probe(0, 0, vx0, vy0)

    while true
        step!(probe)

        if probe.y > maxy
            maxy = probe.y
        end
        if in_target(probe, target)
            return true, maxy
        end
        if past_target(probe, target)
            return false, maxy
        end
    end
end

function step!(probe)
    probe.x += probe.vx
    probe.y += probe.vy
    probe.vx = max(0, probe.vx - 1)
    probe.vy -= 1
end

function in_target(probe, target)
    target.xmin <= probe.x <= target.xmax && target.ymin <= probe.y <= target.ymax
end

function past_target(probe, target)
    probe.x > target.xmax || probe.y < target.ymin
end

main()
