struct Cuboid
    xmin::Int
    xmax::Int
    ymin::Int
    ymax::Int
    zmin::Int
    zmax::Int
end

struct Step
    state::Bool
    cuboid::Cuboid
end

const SMALL = 50

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    steps = parse_steps(lines)

    # PART 1
    apply_steps_naive(steps)

    # PART 2
    apply_steps(steps)
end

function parse_steps(lines)
    map(parse_step, lines)
end

function parse_step(line)
    state, coordinates = split(line, " ")
    state = state == "on"

    x, y, z = map(s -> s[3:end], split(coordinates, ","))
    xmin, xmax = parse.(Int, split(x, ".."))
    ymin, ymax = parse.(Int, split(y, ".."))
    zmin, zmax = parse.(Int, split(z, ".."))

    return Step(state, Cuboid(xmin, xmax, ymin, ymax, zmin, zmax))
end

function apply_steps_naive(steps)
    # this will never work for part 2
    steps = filter(s -> is_small_cuboid(s.cuboid), steps)  # only use small cuboids
    M = falses(2 * SMALL + 1, 2 * SMALL + 1, 2 * SMALL + 1)

    for step in steps
        apply_step_naive!(M, step)
    end

    println("How many cubes are on?")
    println(sum(M))
end

function apply_step_naive!(M, step)
    offset = SMALL + 1  # transform -50..50 <-> 1..101
    cuboid = step.cuboid
    M[cuboid.xmin+offset:cuboid.xmax+offset,
      cuboid.ymin+offset:cuboid.ymax+offset,
      cuboid.zmin+offset:cuboid.zmax+offset] .= step.state
end

function is_small_cuboid(cuboid)
    cuboid.xmin >= -SMALL && cuboid.xmax <= SMALL &&
        cuboid.ymin >= -SMALL && cuboid.ymax <= SMALL &&
        cuboid.zmin >= -SMALL && cuboid.zmax <= SMALL
end

function apply_steps(steps)
    lit_cuboids = []

    for step in steps
        lit_cuboids = apply_step(lit_cuboids, step)
    end

    println("How many cubes are on?")
    println(sum(map(cuboid_size, lit_cuboids)))
end

function apply_step(lit_cuboids, step)
    # check if the new cuboid intersects any of the lit cuboids
    new_lit_cuboids = Cuboid[]
    for lit_cuboid in lit_cuboids
        diff = difference_cuboids(lit_cuboid, step.cuboid)
        new_lit_cuboids = vcat(new_lit_cuboids, diff)
    end
    if step.state
        push!(new_lit_cuboids, step.cuboid)
    end
    return new_lit_cuboids
end

function intersect_cuboids(c1, c2)
    if c1.xmin > c2.xmax || c1.xmax < c2.xmin ||
        c1.ymin > c2.ymax || c1.ymax < c2.ymin ||
        c1.zmin > c2.zmax || c1.zmax < c2.zmin
        # the new cuboid does not intersect this lit cuboid
        return false
    end
    return Cuboid(max(c1.xmin, c2.xmin), min(c1.xmax, c2.xmax),
                  max(c1.ymin, c2.ymin), min(c1.ymax, c2.ymax),
                  max(c1.zmin, c2.zmin), min(c1.zmax, c2.zmax))
end

function difference_cuboids(c1, c2)
    int = intersect_cuboids(c1, c2)
    if int === false
        return c1
    end
    new_cuboids = [
        Cuboid(c1.xmin, c1.xmax, c1.ymin, c1.ymax, c1.zmin, int.zmin - 1),
        Cuboid(c1.xmin, c1.xmax, c1.ymin, c1.ymax, int.zmax + 1, c1.zmax),
        Cuboid(c1.xmin, c1.xmax, c1.ymin, int.ymin - 1, int.zmin, int.zmax),
        Cuboid(c1.xmin, c1.xmax, int.ymax + 1, c1.ymax, int.zmin, int.zmax),
        Cuboid(c1.xmin, int.xmin - 1, int.ymin, int.ymax, int.zmin, int.zmax),
        Cuboid(int.xmax + 1, c1.xmax, int.ymin, int.ymax, int.zmin, int.zmax)
    ]
    filter!(c -> c.xmin <= c.xmax && c.ymin <= c.ymax && c.zmin <= c.zmax, new_cuboids)
    return new_cuboids
end

function cuboid_size(c)
    (c.xmax - c.xmin + 1) * (c.ymax - c.ymin + 1) * (c.zmax - c.zmin + 1)
end

main()
