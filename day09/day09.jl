const MAX_HEIGHT = 9

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    heightmap = generate_heightmap(lines)

    # PART 1
    risk_levels(heightmap)

    # PART 2
    basin_sizes(heightmap)
end

function generate_heightmap(lines)
    cols = length(lines[1])
    rows = length(lines)

    heightmap = Matrix{Int}(undef, rows, cols)
    for i = 1:rows
        heightmap[i, :] = parse.(Int, collect(lines[i]))
    end

    return heightmap
end

function risk_levels(heightmap)
    risk_level = sum(map(p -> heightmap[p] + 1,
                     filter(p -> is_low_point(heightmap, p), CartesianIndices(heightmap))))

    println("What is the sum of the risk levels of all low points on your heightmap?")
    println(risk_level)
end

function is_low_point(heightmap, point)
    # check whether a point is a low point
    all(heightmap[get_neighbours(heightmap, point)] .> heightmap[point])
end

function get_neighbours(heightmap, point)
    # get the neighbouring points of a point on the heightmap
    neighbours = CartesianIndex{2}[]

    if point[1] > 1  # has a neighbour above
        push!(neighbours, CartesianIndex(point[1] - 1, point[2]))
    end
    if point[1] < size(heightmap, 1)  # has a neighbour below
        push!(neighbours, CartesianIndex(point[1] + 1, point[2]))
    end
    if point[2] > 1  # has a neighbour to the left
        push!(neighbours, CartesianIndex(point[1], point[2] - 1))
    end
    if point[2] < size(heightmap, 2)  # has a neighbour to the right
        push!(neighbours, CartesianIndex(point[1], point[2] + 1))
    end
    return neighbours
end

function basin_sizes(heightmap)
    # compute the multiple of the sizes of the three largest basins
    sizes = map(p -> get_basin_size(heightmap, p),
                filter(p -> is_low_point(heightmap, p), CartesianIndices(heightmap)))

    println("What do you get if you multiply together the sizes of the three largest basins?")
    println(prod(sort(sizes, rev=true)[1:3]))
end

function get_basin_size(heightmap, low_point)
    # compute the size of a basin, given its low point
    points_in_basin = Set{CartesianIndex{2}}([low_point])
    points_to_check = [low_point]

    while length(points_to_check) > 0
        # get a point
        point = pop!(points_to_check)

        neighbours = get_neighbours(heightmap, point)

        # check the neighbours; if higher and not MAX_HEIGHT, add to basin
        for nb in neighbours
            if !(nb in points_in_basin) && heightmap[nb] > heightmap[point] && heightmap[nb] != MAX_HEIGHT
                push!(points_in_basin, nb)
                push!(points_to_check, nb)
            end
        end
    end

    return length(points_in_basin)
end

main()
