struct Image
    pixels::BitMatrix
    infinite::Bool  # output towards infinite directions
end

const CONVOLUTION_MATRIX = 2 .^ transpose(reshape(collect(8:-1:0), (3, 3)))

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    ruleset = parse_ruleset(lines[1])
    initial_image = parse_image(lines[3:end])

    # PART 1
    enhance(2, ruleset, initial_image)

    # PART 2
    enhance(50, ruleset, initial_image)
end

function parse_ruleset(line)
    BitArray(map(c -> c == '#', collect(line)))
end

function parse_image(lines)
    # two padding pixels around the image for the convolution
    cols = length(lines[1]) + 4
    rows = length(lines) + 4

    pixels = falses(rows, cols)
    for i = 1:length(lines)
        pixels[i + 2, 3:end-2] = map(c -> c == '#', collect(lines[i]))
    end

    return Image(pixels, false)
end

function enhance(n, ruleset, image)
    for _ = 1:n
        image = enhance(ruleset, image)
    end

    println("How many pixels are lit in the resulting image?")
    println(sum(image.pixels))
end

function enhance(ruleset, image)
    if image.infinite
        new_pixels = falses(size(image.pixels))
    else
        new_pixels = trues(size(image.pixels))
    end

    # perform the convolution
    indices = CartesianIndices(image.pixels)[2:end-1, 2:end-1]  # don't need the outside border
    I = oneunit(indices[1])

    for index in indices
        region = index-I:index+I
        new_pixels[index] = convolve(ruleset, image.pixels[region])
    end

    # convolve the infinite part of the image
    new_infinite = (image.infinite) ? ruleset[end] : ruleset[1]

    # add extra padding to pixels
    if new_infinite
        new_pixels_padded = trues(size(new_pixels) .+ 2)
    else
        new_pixels_padded = falses(size(new_pixels) .+ 2)
    end
    new_pixels_padded[2:end-1, 2:end-1] = new_pixels

    return Image(new_pixels_padded, new_infinite)
end

function convolve(ruleset, region_pixels)
    ruleset[sum(region_pixels .* CONVOLUTION_MATRIX) + 1]
end

main()
