const START_NODE = "start"
const END_NODE = "end"

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    cave_graph = generate_cave_graph(lines)

    # PART 1
    visit_small_caves_once(cave_graph)

    # PART 2
    visit_one_cave_twice(cave_graph)
end

function generate_cave_graph(lines)
    cave_graph = Dict{String, Set{String}}()
    for line in lines
        v1, v2 = split(line, '-')
        if !(v1 in keys(cave_graph))
            cave_graph[v1] = Set{String}()
        end
        push!(cave_graph[v1], v2)
        if !(v2 in keys(cave_graph))
            cave_graph[v2] = Set{String}()
        end
        push!(cave_graph[v2], v1)
    end
    return cave_graph
end

function visit_small_caves_once(cave_graph)
    num_paths = depth_first_search(cave_graph, START_NODE)

    println("How many paths through this cave system are there that visit small caves at most once?")
    println(num_paths)
end

function visit_one_cave_twice(cave_graph)
    num_paths = depth_first_search(cave_graph, START_NODE, another_visit=true)

    println("Given these new rules, how many paths through this cave system are there?")
    println(num_paths)
end

function depth_first_search(cave_graph, node, visited=Set{String}(); another_visit=false)
    # return the number of paths that can be taken from node to the end node
    if node == END_NODE
        return 1
    end
    if is_small_cave(node)
        push!(visited, node)
    end
    total = 0
    for neighbour in cave_graph[node]
        if !(neighbour in visited)
            total += depth_first_search(cave_graph, neighbour, copy(visited); another_visit)
        elseif another_visit && neighbour != START_NODE
            # ignore visited neighbour and toggle another_visit
            total += depth_first_search(cave_graph, neighbour, copy(visited); another_visit=false)
        end
    end
    return total
end

function is_small_cave(node)
    all(c -> islowercase(c), node)
end

main()
