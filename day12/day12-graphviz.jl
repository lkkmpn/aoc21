function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    cave_graph = generate_cave_graph(lines)
    dot_contents = generate_dot(cave_graph)

    open("graph.dot", "w") do io
        write(io, dot_contents)
    end
end

function generate_cave_graph(lines)
    cave_graph = Dict{String, Set{String}}()
    for line in lines
        v1, v2 = split(line, '-')
        if !(v1 in keys(cave_graph))
            cave_graph[v1] = Set{String}()
        end
        push!(cave_graph[v1], v2)
        if !(v2 in keys(cave_graph))
            cave_graph[v2] = Set{String}()
        end
        push!(cave_graph[v2], v1)
    end
    return cave_graph
end

function generate_dot(cave_graph)
    dot_contents = "digraph {\n"
    dot_contents *= "    concentrate=true\n"
    dot_contents *= "    subgraph {rank=\"min\"; start}\n"
    dot_contents *= "    subgraph {rank=\"max\"; end}\n"
    for node in cave_graph
        dot_contents *= "    " * node.first * " -> {" * join(node.second, " ") * "}\n"
    end
    dot_contents *= "}\n"
    return dot_contents
end

main()
