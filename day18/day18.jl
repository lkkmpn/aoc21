function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    numbers = parse_number.(lines)

    # PART 1
    do_homework(numbers)

    # PART 2
    do_the_back_of_the_homework(numbers)
end

function parse_number(line)
    # none of this binary tree nonsense, we just store everything as a flat list
    map(c -> isdigit(c) ? parse(Int, c) : c,
        collect(replace(line, "," => "")))
end

function do_homework(numbers)
    result = add_numbers(numbers)
    magnitude = reduce_magnitude(result)

    println("What is the magnitude of the final sum?")
    println(magnitude)
end

function do_the_back_of_the_homework(numbers)
    largest_magnitude = 0
    # loop over all pairs
    for i = 1:length(numbers), j = 1:length(numbers)
        if i == j
            continue
        end
        magnitude = reduce_magnitude(add_numbers([numbers[i], numbers[j]]))
        if magnitude > largest_magnitude
            largest_magnitude = magnitude
        end
    end

    println("What is the largest magnitude of any sum of two different snailfish numbers from the homework assignment?")
    println(largest_magnitude)
end

function add_numbers(numbers)
    result = numbers[1]
    for number in numbers[2:end]
        result = add_pair(result, number)
        reduce_number!(result)
    end
    return result
end

function add_pair(a, b)
    vcat(['['], a, b, [']'])
end

function reduce_number!(number)
    while true
        idx_to_explode = find_explode(number)
        if idx_to_explode !== false
            explode!(number, idx_to_explode)
            continue
        end
        idx_to_split = find_split(number)
        if idx_to_split !== false
            split_number!(number, idx_to_split)
            continue
        end
        break
    end
end

function find_explode(number)
    depth = 0
    for (i, c) in enumerate(number)
        if c == '['
            depth += 1
        elseif c == ']'
            depth -= 1
        end
        if depth > 4
            return i
        end
    end
    return false
end

function explode!(number, idx)
    left, right = number[idx + 1], number[idx + 2]

    # find first regular number to the left
    for i = idx:-1:1
        if number[i] isa Int
            number[i] += left
            break
        end
    end

    # find first regular number to the right
    for i = idx+3:length(number)
        if number[i] isa Int
            number[i] += right
            break
        end
    end

    # replace pair with 0
    number[idx] = 0
    deleteat!(number, idx+1:idx+3)
end

function find_split(number)
    for (i, c) in enumerate(number)
        if c isa Int && c >= 10
            return i
        end
    end
    return false
end

function split_number!(number, idx)
    v = number[idx]
    pair = ['[', floor(Int, v / 2), ceil(Int, v / 2), ']']
    deleteat!(number, idx)
    for (i, c) in enumerate(pair)
        insert!(number, idx + i - 1, c)
    end
end

function reduce_magnitude(number)
    # solve until we only have a single number left
    while length(number) > 1
        # work in reverse so indices don't change
        for i = length(number)-2:-1:1
            if number[i + 1] isa Int && number[i + 2] isa Int  # start of pair
                magnitude = 3 * number[i + 1] + 2 * number[i + 2]
                number[i] = magnitude
                deleteat!(number, i+1:i+3)
            end
        end
    end
    return number[1]
end

main()
