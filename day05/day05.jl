# let's try this approach
struct Point
    x::Int
    y::Int
end

struct Segment
    p1::Point
    p2::Point
end

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    # get data from input file
    line_segments = get_line_segments(lines)

    part_one(line_segments)
    part_two(line_segments)
end

function get_line_segments(lines)
    # get all line segments from input file
    line_segments = Segment[]

    for line in lines
        coordinates = split.(split(line, " -> "), ",")
        x1 = Point(parse.(Int, coordinates[1])...)  # ...: unpack
        x2 = Point(parse.(Int, coordinates[2])...)
        line_segment = Segment(x1, x2)
        push!(line_segments, line_segment)
    end

    return line_segments
end

function part_one(line_segments)
    # filter out only horizontal and vertical (Euclidean) line segments
    euclidean_line_segments = filter(s -> s.p1.x == s.p2.x || s.p1.y == s.p2.y, line_segments)
    overlapping_points = get_overlapping_points(euclidean_line_segments)

    println("At how many points do at least two lines overlap?")
    println(overlapping_points)
end

function part_two(line_segments)
    overlapping_points = get_overlapping_points(line_segments)

    println("At how many points do at least two lines overlap?")
    println(overlapping_points)
end

function get_overlapping_points(line_segments)
    points = get_covered_points(line_segments)
    count = count_point_occurrences(points)
    overlapping_points = count_overlapping_points(count)

    return overlapping_points
end

function get_covered_points(line_segments)
    points = Point[]
    for s in line_segments
        # horizontal: ydir = 0
        # vertical: xdir = 0
        # diagonal: xdir, ydir = ±1
        length = max(abs(s.p2.x - s.p1.x), abs(s.p2.y - s.p1.y))
        xdir = Int((s.p2.x - s.p1.x) / length)
        ydir = Int((s.p2.y - s.p1.y) / length)
        for i = 0:length
            push!(points, Point(s.p1.x + xdir * i, s.p1.y + ydir * i))
        end
    end
    return points
end

function count_point_occurrences(points)
    count = Dict{Point, Int}()
    for point in points
        if point in keys(count)
            count[point] += 1
        else
            count[point] = 1
        end
    end
    return count
end

function count_overlapping_points(count)
    length(filter(c -> c.second > 1, count))
end

main()
