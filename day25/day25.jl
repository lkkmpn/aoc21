const EMPTY = 0
const EAST = 1
const SOUTH = 2

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    ocean_map = parse_ocean_map(lines)

    step_until_done!(ocean_map)
end

function parse_ocean_map(lines)
    cols = length(lines[1])
    rows = length(lines)

    ocean_map = zeros(Int8, rows, cols)
    for i = 1:rows
        ocean_map[i, :] = parse.(Int, collect(replace(lines[i], '.' => EMPTY, '>' => EAST, 'v' => SOUTH)))
    end

    return ocean_map
end

function step_until_done!(ocean_map)
    steps = 0
    while true
        prev_ocean_map = copy(ocean_map)
        step!(ocean_map)
        steps += 1

        if all(ocean_map - prev_ocean_map .== 0)
            break
        end
    end

    println("What is the first step on which no sea cucumbers move?")
    println(steps)
end

function step!(ocean_map)
    step_direction!(ocean_map, EAST)
    step_direction!(ocean_map, SOUTH)
end

function step_direction!(ocean_map, direction)
    direction_facing = ocean_map .== direction
    free_direction = shift_matrix(ocean_map, -direction) .== EMPTY
    move_direction = direction_facing .& free_direction

    ocean_map[move_direction] .= 0
    ocean_map[shift_matrix(move_direction, direction)] .= direction
end

function shift_matrix(M, direction)
    # [1] to keep dimensionality in the slice (2D matrix)
    if direction == -EAST
        return hcat(M[:, 2:end], M[:, [1]])
    elseif direction == EAST
        return hcat(M[:, [end]], M[:, 1:end-1])
    elseif direction == -SOUTH
        return vcat(M[2:end, :], M[[1], :])
    elseif direction == SOUTH
        return vcat(M[[end], :], M[1:end-1, :])
    end
    return M
end

main()
