function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    template = collect(lines[1])
    rules = parse_rules(lines[3:end])

    part_one(template, rules)
    part_two(template, rules)
end

function parse_rules(lines)
    rules = Dict{String, Char}()
    for line in lines
        a, b = split(line, " -> ")
        # only: convert String to Char
        rules[a] = only(b)
    end
    return rules
end

function part_one(template, rules)
    polymer = copy(template)  # polymer is changed in-place

    for _ = 1:10  # 10 steps in this part
        naive_step!(polymer, rules)
    end

    count = count_elements(polymer)
    diff = maximum(values(count)) - minimum(values(count))

    println("What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?")
    println(diff)
end

function naive_step!(polymer, rules)
    # work backwards, so that indexing is not affected
    for i in length(polymer)-1:-1:1
        pair = polymer[i] * polymer[i + 1]
        insert!(polymer, i + 1, rules[pair])
    end
end

function count_elements(polymer)
    count = Dict{Char, Int}()
    for element in polymer
        add_to_dict_key!(count, element, 1)
    end
    return count
end

function part_two(template, rules)
    # the naive approach doesn't work anymore
    # instead, we track the number of pairs in the polymer
    # this means all elements are counted double, except for the one at the
    # beginning and the one at the end, which never change, hence they can be
    # extracted from the template
    pair_count = setup_pair_count_list(template, rules)

    for _ = 1:40  # 40 steps in this part
        pair_count = better_step(pair_count, rules)
    end

    # convert pair count to element count
    count = get_element_count(pair_count, template)
    diff = maximum(values(count)) - minimum(values(count))

    println("What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?")
    println(diff)
end

function setup_pair_count_list(template, rules)
    # set up a pair count list with all possible pairs (given by rules)
    pair_count = Dict(keys(rules) .=> 0)

    # initialise pair count with the pairs in the template
    for i in length(template)-1:-1:1
        pair = template[i] * template[i + 1]
        pair_count[pair] += 1
    end

    return pair_count
end

function better_step(pair_count, rules)
    new_pair_count = Dict(keys(rules) .=> 0)

    # if an element is inserted between a pair, new pairs are formed with counts
    # equal to the count of the original pair
    for pair in keys(pair_count)
        new_pair_count[pair[1] * rules[pair]] += pair_count[pair]
        new_pair_count[rules[pair] * pair[2]] += pair_count[pair]
    end

    return new_pair_count
end

function get_element_count(pair_count, template)
    # convert pair count back to element count

    count = Dict{Char, Int}()
    for pair in keys(pair_count)
        add_to_dict_key!(count, pair[1], pair_count[pair])
        add_to_dict_key!(count, pair[2], pair_count[pair])
    end

    # add beginning and end
    count[template[1]] += 1
    count[template[end]] += 1

    # correct for double-counting
    for element in keys(count)
        count[element] /= 2
    end

    return count
end

function add_to_dict_key!(d, k, v)
    if k in keys(d)
        d[k] += v
    else
        d[k] = v
    end
end

main()
