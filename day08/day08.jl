function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    # PART 1
    easy_digits_puzzle(lines)

    # PART 2
    harder_digits_puzzle(lines)
end

function easy_digits_puzzle(lines)
    output_values = map(line -> line[2], split.(lines, " | "))
    count = sum(count_easy_digits_in_output.(output_values))

    println("In the output values, how many times do digits 1, 4, 7, or 8 appear?")
    println(count)
end

function count_easy_digits_in_output(output)
    # count the number of 1, 4, 7, 8 digits in the output value
    num_segments = length.(split(output))
    sum(num_segments .== 2 .||  # 1
        num_segments .== 4 .||  # 4
        num_segments .== 3 .||  # 7
        num_segments .== 7)     # 8
end

function harder_digits_puzzle(lines)
    sum_values = sum(decode_output_value.(lines))

    println("What do you get if you add up all of the output values?")
    println(sum_values)
end

function decode_output_value(line)
    patterns_str, output_value_str = split(line, " | ")
    patterns_list = Set.(split(patterns_str))
    output_value_list = Set.(split(output_value_str))

    patterns = decode_unique_patterns(patterns_list)

    # decode output value
    # passing patterns as tuple ensures broadcasting over output_value_list
    output_digits = decode_single_value.((patterns,), output_value_list)

    return sum(map(v -> v[2] * 10 ^ (length(output_digits) - v[1]), enumerate(output_digits)))
end

function decode_unique_patterns(patterns_list)
    # deduce the digits corresponding to the unique signal patterns
    patterns = Dict{Int, Set{Char}}()

    # extract the easy digits
    for pattern in patterns_list
        if length(pattern) == 2
            patterns[1] = pattern
        elseif length(pattern) == 4
            patterns[4] = pattern
        elseif length(pattern) == 3
            patterns[7] = pattern
        elseif length(pattern) == 7
            patterns[8] = pattern
        end
    end

    # 5-signal patterns: 2, 3, 5
    patterns_list_5 = filter(p -> length(p) == 5, patterns_list)
    # 3: the intersection of 3 and 7 must return 3 signals
    patterns[3] = first(filter(p -> length(intersect(p, patterns[7])) == 3, patterns_list_5))
    # 2: the intersection of 2 and 4 must return 2 signals
    patterns[2] = first(filter(p -> length(intersect(p, patterns[4])) == 2, patterns_list_5))
    # 5: remaining pattern in the list
    patterns[5] = first(filter(p -> !(p in values(patterns)), patterns_list_5))

    # 6-signal patterns: 6, 9, 0
    patterns_list_6 = filter(p -> length(p) == 6, patterns_list)
    # 6: the intersection of 6 and 1 must return 1 signal
    patterns[6] = first(filter(p -> length(intersect(p, patterns[1])) == 1, patterns_list_6))
    # 9: the intersection of 9 and 4 must return 4 signals
    patterns[9] = first(filter(p -> length(intersect(p, patterns[4])) == 4, patterns_list_6))
    # 0: remaining pattern in the list
    patterns[0] = first(filter(p -> !(p in values(patterns)), patterns_list_6))

    return patterns
end

function decode_single_value(patterns, signals)
    # decode a single output value using deduced patterns
    first(filter(k -> issetequal(patterns[k], signals), keys(patterns)))
end

function sort_characters(str)
    join(sort(collect(str)))
end

main()
