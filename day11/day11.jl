const FLASH_THRESHOLD = 9

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    energy_levels = generate_energy_levels(lines)

    # PART 1
    flashes_after_steps(energy_levels, 100)

    # PART 2
    steps_to_synchronise(energy_levels)
end

function generate_energy_levels(lines)
    cols = length(lines[1])
    rows = length(lines)

    energy_levels = Matrix{Int}(undef, rows, cols)
    for i = 1:rows
        energy_levels[i, :] = parse.(Int, collect(lines[i]))
    end
    return energy_levels
end

function flashes_after_steps(initial_input, num_steps)
    energy_levels = copy(initial_input)  # copy because step works in-place
    total_flashes = 0
    for _ = 1:num_steps
        total_flashes += step!(energy_levels)
    end

    println("How many total flashes are there after 100 steps?")
    println(total_flashes)
end

function steps_to_synchronise(initial_input)
    energy_levels = copy(initial_input)  # copy because step works in-place
    num_flashes, step = 0, 0
    while num_flashes != length(energy_levels)
        num_flashes = step!(energy_levels)
        step += 1
    end

    println("What is the first step during which all octopuses flash?")
    println(step)
end

function step!(energy_levels)
    # perform one step, change the energy levels in-place and return the number of flashes that occurred
    indices = CartesianIndices(energy_levels)
    i_first, i_last = first(indices), last(indices)
    I = oneunit(i_first)

    flashed = falses(size(energy_levels)...)  # an octopus can only flash at most once per step

    energy_levels .+= 1  # first, the energy level of each octopus increases by 1

    # then, we enter recursive flashing
    flashing_indices = findall(i -> energy_levels[i] > FLASH_THRESHOLD, indices)
    while length(flashing_indices) > 0
        flashed[flashing_indices] .= true  # an octopus can only flash at most once per step

        # increase the energy level of adjacent octopuses
        # using the smart trick from https://julialang.org/blog/2016/02/iteration/
        for flashing_index in flashing_indices
            for neighbour in max(i_first, flashing_index - I):min(i_last, flashing_index + I)
                energy_levels[neighbour] += 1
            end
        end

        flashing_indices = findall(i -> energy_levels[i] > FLASH_THRESHOLD && !flashed[i], indices)
    end

    # reset energy levels of octopuses which have flashed
    energy_levels[flashed .== true] .= 0

    return sum(flashed)
end

main()
