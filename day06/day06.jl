const NEW_FISH_TIMER = 8
const RESET_FISH_TIMER = 6

function main()
    line = open("input.txt") do io
        read(io, String)
    end

    initial_input = parse.(Int, split(line, ","))

    part_one(initial_input)
    part_two(initial_input)
end

function part_one(initial_input)
    # naive approach, tracking all fish independently
    lanternfish = copy(initial_input)  # copy because process_fish works in-place

    for _ = 1:80
        process_fish!(lanternfish)
    end

    println("Hoe many lanternfish would there be after 80 days?")
    println(length(lanternfish))
end

function process_fish!(fishes)
    for i = 1:length(fishes)
        if fishes[i] == 0  # new fish is born
            push!(fishes, NEW_FISH_TIMER)
        end
        fishes[i] -= 1
        if fishes[i] < 0
            fishes[i] = RESET_FISH_TIMER
        end
    end
end

function part_two(initial_input)
    # the naive approach is now way too memory-intensive, so we need a smarter approach
    # keep track of the number of fish with internal timer set to a certain value
    fish_count = initialise_fish_count(initial_input)

    for _ = 1:256
        process_fish_smart!(fish_count)
    end

    println("Hoe many lanternfish would there be after 256 days?")
    println(sum(values(fish_count)))
end

function initialise_fish_count(lanternfish)
    fish_count = Dict{Int, Int}()
    for i = 0:NEW_FISH_TIMER  # initialise all zeros
        fish_count[i] = 0
    end
    for fish in lanternfish
        fish_count[fish] += 1
    end
    return fish_count
end

function process_fish_smart!(fish_count)
    # store number of spawned fish
    spawn = fish_count[0]
    # subtract 1 from all positive timers
    for i in 0:NEW_FISH_TIMER-1
        fish_count[i] = fish_count[i + 1]
    end
    fish_count[RESET_FISH_TIMER] += spawn  # reset 0 to RESET_FISH_TIMER
    fish_count[NEW_FISH_TIMER] = spawn
end

main()
