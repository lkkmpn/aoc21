using LinearAlgebra

const MATCH_THRESHOLD = 12

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    scanners = parse_scanners(lines)

    # use the first scanner's orientation as reference
    matched_scanners = Set(1)
    scanner_positions = Vector[[0, 0, 0]]

    while true
        # try to match scanners
        for i in matched_scanners
            for j in filter(j -> !(j in matched_scanners), 1:length(scanners))
                match = match_scanners!(scanners, i, j)
                if match !== false
                    push!(matched_scanners, j)
                    push!(scanner_positions, match)
                end
            end
        end
        if length(matched_scanners) == length(scanners)
            break
        end
    end

    beacons = Set{Vector{Int}}()
    for i in 1:length(scanners)
        union!(beacons, Set(scanners[i]))
    end

    println("How many beacons are there?")
    println(length(beacons))

    max_distance = 0
    for i in 1:length(scanner_positions), j in i+1:length(scanner_positions)
        scanner_a, scanner_b = scanner_positions[i], scanner_positions[j]
        dist = sum(map(x -> abs(x[2] - x[1]), zip(scanner_a, scanner_b)))
        if dist > max_distance
            max_distance = dist
        end
    end

    println("What is the largest Manhattan distance between any two scanners?")
    println(max_distance)
end

function parse_scanners(lines)
    scanners = Vector[]
    current_scanner = Vector[]
    for line in lines
        if length(strip(line)) == 0  # current scanner is finished
            push!(scanners, current_scanner)
        elseif occursin("---", line)  # new scanner
            current_scanner = Vector[]
        else  # coordinates
            push!(current_scanner, parse.(Int, split(line, ",")))
        end
    end
    push!(scanners, current_scanner)  # final scanner
    return scanners
end

function match_scanners!(scanners, i, j)
    scanner_a, scanner_b = scanners[i], scanners[j]

    # scanner a is reference, scanner b is transformed
    scanner_b_rots = rotations(scanner_b)

    for scanner_b_rot in scanner_b_rots
        for beacon_a in scanner_a, beacon_b in scanner_b_rot
            # if beacon_a and beacon_b are the same, then their difference
            # vector is the distance between scanner_a and scanner_b_rot
            diff = beacon_a .- beacon_b

            # add this difference vector to all beacons detected by scanner_b_rot
            translated_b_rot = map(b -> b .+ diff, scanner_b_rot)

            # if at least 12 beacons match, we know the offset and basis
            if length(intersect(Set(scanner_a), Set(translated_b_rot))) >= MATCH_THRESHOLD
                # we return the offset, and set the beacon coordinates to be reference coordinates
                scanners[j] = translated_b_rot
                return diff
            end
        end
    end

    return false
end

function rotations(beacons)
    # rotate a scanner's output into all 24 permutations
    rotated = Vector[]
    us = [
        [1, 0, 0],
        [-1, 0, 0],
        [0, 1, 0],
        [0, -1, 0],
        [0, 0, 1],
        [0, 0, -1]
    ]
    for v1 in us, v2 in us
        if dot(v1, v2) == 0  # perpendicular vectors
            v3 = cross(v1, v2)
            M = hcat(v1, v2, v3)
            push!(rotated, map(s -> M * s, beacons))
        end
    end
    return rotated
end

main()
