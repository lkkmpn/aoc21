function main()
    line = open("input.txt") do io
        read(io, String)
    end

    transmission = parse_transmission(line)

    version_sum, value, _ = read_packet(transmission)

    println("What do you get if you add up the version numbers in all packets?")
    println(version_sum)

    println("What do you get if you evaluate the expression represented by your hexadecimal-encoded BITS transmission?")
    println(value)
end

function parse_transmission(line)
    # return the message in binary representation as string
    join(map(c -> lpad(string(parse(Int, c, base=16), base=2), 4, "0"), collect(strip(line))))
end

function read_packet(transmission, offset=0)
    base_offset = offset
    version = read_int(transmission, offset, 3)
    type_id = read_int(transmission, offset + 3, 3)
    offset += 6

    if type_id == 4
        value, bits_read = parse_literal_value(transmission, offset)
        return version, value, bits_read + 6
    end

    version_sum = version

    values = Int[]

    length_type = read_int(transmission, offset, 1)
    offset += 1
    if length_type == 0  # total length in bits
        subpackets_length = read_int(transmission, offset, 15)
        offset += 15
        parse_until = offset + subpackets_length
        while offset < parse_until
            v, n, p = read_packet(transmission, offset)
            version_sum += v
            push!(values, n)
            offset += p
        end
    else  # number of subpackets
        num_subpackets = read_int(transmission, offset, 11)
        offset += 11
        for _ in 1:num_subpackets
            v, n, p = read_packet(transmission, offset)
            version_sum += v
            push!(values, n)
            offset += p
        end
    end

    if type_id == 0
        value = sum(values)
    elseif type_id == 1
        value = prod(values)
    elseif type_id == 2
        value = minimum(values)
    elseif type_id == 3
        value = maximum(values)
    elseif type_id == 5
        value = (values[1] > values[2]) ? 1 : 0
    elseif type_id == 6
        value = (values[1] < values[2]) ? 1 : 0
    elseif type_id == 7
        value = (values[1] == values[2]) ? 1 : 0
    end

    return version_sum, value, offset - base_offset
end

function read_int(transmission, offset, length)
    parse(Int, transmission[offset + 1:offset + length], base=2)
end

function read_bits(transmission, offset, length)
    transmission[offset + 1:offset + length]
end

function read_bit(transmission, offset)
    parse(Bool, transmission[offset + 1])
end

function parse_literal_value(transmission, base_offset)
    # type id = 4
    offset = 0
    value_bits = []
    while read_bit(transmission, base_offset + offset)
        push!(value_bits, read_bits(transmission, base_offset + offset + 1, 4))
        offset += 5
    end
    push!(value_bits, read_bits(transmission, base_offset + offset + 1, 4))  # final bit
    bits_read = offset + 5
    return parse(Int, join(value_bits), base=2), bits_read
end

main()
