function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    variables = extract_monad_variables(lines)

    part_one(variables)
    part_two(variables)
end

function extract_monad_variables(lines)
    # the MONAD code repeats 18 operations, and in each of these iterations,
    # 3 operations contain a variable input (different for each iteration)
    # specifically:
    # 5  div z [value]
    # 6  add x [value]
    # 16 add y [value]
    div_z = map(l -> parse(Int, split(l, " ")[end]), lines[5:18:end])
    add_x = map(l -> parse(Int, split(l, " ")[end]), lines[6:18:end])
    add_y = map(l -> parse(Int, split(l, " ")[end]), lines[16:18:end])
    return div_z, add_x, add_y
end

function monad(variables, input)
    # unused, but left for testing
    div_z, add_x, add_y = variables
    x, y, z = 0, 0, 0

    for (i, inp) in enumerate(input)
        x = z % 26
        z = Int(floor(z / div_z[i]))
        temp = x + add_x[i]
        x = (temp == inp) ? 0 : 1
        y = (temp == inp) ? 1 : 26
        z *= y
        y = (inp + add_y[i]) * x
        z += y
    end

    return z == 0
end

function part_one(variables)
    input = solve(variables, 9:-1:1)

    println("What is the largest model number accepted by MONAD?")
    println(join(input))
end

function part_two(variables)
    input = solve(variables, 1:1:9)

    println("What is the smallest model number accepted by MONAD?")
    println(join(input))
end

function solve(variables, range)
    # the only way to make z decrease is by ensuring that, in the iterations
    # with `div z 26`, x is set to 0 (by ensuring `temp == inp`)
    # we have 7 iterations with `div z 1`, so this is brute-forceable

    for i1 in range, i2 in range, i3 in range, i4 in range, i5 in range, i6 in range, i7 in range
        input = [i1, i2, i3, -1, i4, -1, i5, -1, i6, i7, -1, -1, -1, -1]
        z, guessed = monad_guess_input(variables, input)
        if z === 0
            return guessed
        end
    end
end

function monad_guess_input(variables, input)
    div_z, add_x, add_y = variables
    x, y, z = 0, 0, 0

    for (i, inp) in enumerate(input)
        x = z % 26
        z = Int(floor(z / div_z[i]))
        temp = x + add_x[i]
        if inp == -1  # inp should be equal to temp
            if 1 <= temp <= 9
                inp = temp
                input[i] = temp
            else  # impossible to set inp equal to temp
                return false, false
            end
        end
        x = (temp == inp) ? 0 : 1
        y = (temp == inp) ? 1 : 26
        z *= y
        y = (inp + add_y[i]) * x
        z += y
    end

    return z, input
end

main()
