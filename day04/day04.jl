const BOARD_SIZE = 5

function main()
    lines = open("input.txt") do io
        readlines(io)
    end

    # get data from input file
    numbers_drawn = get_numbers_drawn(lines)
    boards = get_boards(lines)

    # set up boolean arrays to store which board positions have been marked
    boards_marked = Matrix{Bool}[]
    for _ in 1:length(boards)
        push!(boards_marked, falses(BOARD_SIZE, BOARD_SIZE))
    end

    # set up arrays to store scores and which boards have won
    scores = Int[]
    boards_won = Int[]

    # draw numbers, and keep track of scores when boards win
    for number_drawn in numbers_drawn
        mark_number!(number_drawn, boards, boards_marked)
        new_boards_won = check_any_boards_won(boards_marked, boards_won)
        append!(boards_won, new_boards_won)

        for new_board_won in new_boards_won
            score = calculate_score(boards[new_board_won], boards_marked[new_board_won], number_drawn)
            push!(scores, score)
        end
    end

    # PART 1: the first board to win
    println("What will your final score be if you choose that board?")
    println(scores[1])

    # PART 2: the last board to win
    println("Once it wins, what would its final score be?")
    println(scores[end])
end

function get_numbers_drawn(lines)
    # get the order in which to draw numbers
    parse.(Int, split(lines[1], ","))
end

function get_boards(lines)
    # get all boards
    num_boards = Int((length(lines) - 1) / (BOARD_SIZE + 1))
    boards = Matrix{Int}[]

    for i = 1:num_boards
        start_line = 6 * (i - 1) + 3
        board = Matrix{Int}(undef, BOARD_SIZE, BOARD_SIZE)

        for j = 1:BOARD_SIZE
            row_line = start_line + j - 1
            row = parse.(Int, split(lines[row_line]))
            board[j, :] = row
        end

        push!(boards, board)
    end

    return boards
end

function mark_number!(number, boards, boards_marked)
    # mark a number on all boards
    for i = 1:length(boards)
        find = findall(x -> x == number, boards[i])
        if length(find) == 1
            boards_marked[i][find[1]] = true
        end
    end
end

function check_any_board_won(boards_marked)
    # check whether any board has won, and return that board number (or false if no board has won)
    for i = 1:length(boards_marked)
        if check_board_won(boards_marked[i])
            return i
        end
    end

    return false
end

function check_any_boards_won(boards_marked, boards_already_won)
    # check whether any boards have won, skipping boards that have already won,
    # and return a list of board numbers that have won
    boards_won = Int[]
    for i = 1:length(boards_marked)
        if i in boards_already_won
            continue
        end
        if check_board_won(boards_marked[i])
            push!(boards_won, i)
        end
    end
    return boards_won
end

function check_board_won(board_marked)
    # if a row or column has been completely marked, its sum is BOARD_SIZE
    any(sum(board_marked, dims=1) .== BOARD_SIZE) || any(sum(board_marked, dims=2) .== BOARD_SIZE)
end

function calculate_score(won_board, won_board_marked, number_drawn)
    sum(won_board[.!won_board_marked]) * number_drawn
end

main()
